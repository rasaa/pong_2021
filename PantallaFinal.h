//
// Created by Christian on 13/07/21.
//

#ifndef PONG2021_PANTALLAFINAL_H
#define PONG2021_PANTALLAFINAL_H

#include <SFML/Graphics.hpp>
#include "Pantalla.h"

class PantallaFinal : public Pantalla {
private:
    sf::Text texto_resultado;

public:
    PantallaFinal(int _pos_x,int _pos_y,int _largo,int _altura,sf::Font *_fuente,sf::RenderWindow *_ventana);
    void eventos(sf::Event evento,std::string &pantalla_actual) override;
    void dibujar() override;
    void actualizar_mensaje(std::string& ganador);

};


#endif //PONG2021_PANTALLAFINAL_H
