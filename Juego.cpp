//
// Created by Christian on 12/07/21.
//


#include "Juego.h"

Juego::Juego() : pos_x(), pos_y(), largo(), altura(), fuente(), ventana(),
            escena1(),escena2(),escena3(),evento(), pantalla_actual() ,ganador(){
    inicializar();
    ejecutar();

}

void Juego::inicializar() {

    // inicializar los atributos

    largo = 800;
    altura = 600;
    pos_x = largo/2;
    pos_y = altura/2;
    pantalla_actual = "inicio";
    ganador = "ganador1o2";
    fuente = new sf::Font();
    if (!fuente->loadFromFile("DATA/FontFile.ttf")) {
        std::cout<<"Fuente de texto no encontrado en DATA"<< std::endl;
        system("pause");
    }
    ventana= new sf::RenderWindow (sf::VideoMode(largo,altura),
                                   "   Desarrollado en C++ con Libreria SFML");
    ventana->setFramerateLimit(60);
    escena1 = new PantallaInicio(pos_x,pos_y,largo,altura,fuente,ventana);
    escena2 = new PantallaJuego(pos_x,pos_y,largo,altura,fuente,ventana);
    escena3 = new PantallaFinal(pos_x,pos_y,largo,altura,fuente,ventana);

}

void Juego::ejecutar() {

    // Bucle Princpal del Juego
    while (ventana->isOpen())
    {
        // Captura los eventos

        while (ventana->pollEvent(evento))
        {
            escena1->eventos( evento, pantalla_actual);
            escena2->eventos( evento, pantalla_actual);
            escena3->eventos(evento,pantalla_actual);

            if (evento.type == sf::Event::Closed)
                ventana->close();
        }

        actualizar();
        adicionarpantalla();        // Se selecciona la escena a mostrar
        ventana->display();         // Finaliza con mostrar en ventana

    }
}

void Juego::actualizar() {

    if (pantalla_actual == "jugando"){
        escena2->detectar_colision();                   // Detectar colision de 2 objetos
        escena2->actualiza_puntos(ganador,pantalla_actual); // Aumenta y verifica los puntos
        escena2->actualizar();                          // Actualiza la posicion de los objetos
    }
    if (pantalla_actual == "final"){
        escena3->actualizar_mensaje(ganador);       // Agrega al ganador en el mensaje final
    }
}

void Juego::adicionarpantalla() {

    if (pantalla_actual == "inicio"){
        escena1->dibujar();
    }
    if (pantalla_actual == "jugando"){
        escena2->dibujar();
    }
    if (pantalla_actual == "final"){
        escena3->dibujar();
    }
}

Juego::~Juego() {
    delete fuente;
    delete ventana;
    delete escena1;
    delete escena2;
    delete escena3;
}


