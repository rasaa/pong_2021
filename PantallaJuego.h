//
// Created by Christian on 13/07/21.
//

#ifndef PONG2021_PANTALLAJUEGO_H
#define PONG2021_PANTALLAJUEGO_H

#include <SFML/Graphics.hpp>
#include "Pantalla.h"
#include "Jugador.h"
#include "Pelota.h"
#include <random>

const int largo_figura = 20;
const int altura_figura = 50;
const int radio_circulo = 15;
const int velocidad_horizontal = 4;
const int velocidad_vertical = 4;

class PantallaJuego : public Pantalla{
private:
    sf::RectangleShape malla;
    Jugador jugador1;
    Jugador jugador2;
    Pelota pelota;
    std::random_device r;
public:
    PantallaJuego(int _pos_x, int _pos_y,int _largo, int _altura,sf::Font * _fuente,sf::RenderWindow * _ventana);
    void dibujar_malla();
    void dibujar_puntos();
    void actualiza_puntos(std::string& ganador,std::string& pantalla_actual);
    void actualizar();
    void dibujar() override;
    void eventos(sf::Event evento, std::string &pantalla_actual) override;
    void detectar_colision();
    void posicion_inicio();

};


#endif //PONG2021_PANTALLAJUEGO_H
