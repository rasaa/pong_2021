//
// Created by Christian on 20/07/21.
//

#include "Jugador.h"

Jugador::Jugador(int _largo, int _altura, sf::RenderWindow *_ventana)
                : Figura(_largo,_altura,_ventana),puntos(0){

    raqueta.setSize(sf::Vector2f(largo,altura));    // Se define tamaño de la raqueta
    raqueta.setFillColor(sf::Color::White);             // Se define color Blanco de la raqueta

}

void Jugador::mover() {

    // La raqueta se desplaza solo en el eje Y
    if ( pos_y + des_y <= 0){
        des_y = 0;
    }
    if (pos_y + des_y + altura >= ventana->getSize().y){
        des_y =0;
    }

    // Suma la velocidad a la posicion en Y
    pos_y += des_y;
}

void Jugador::dibujar() {

    raqueta.setPosition((float )pos_x,(float )pos_y);
    ventana->draw(raqueta);
}

int Jugador::get_puntos() {
    return puntos;
}
