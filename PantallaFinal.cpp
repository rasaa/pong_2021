//
// Created by Christian on 13/07/21.
//

#include "PantallaFinal.h"
#include "iostream"

PantallaFinal::PantallaFinal(int _pos_x, int _pos_y, int _largo, int _altura,
                             sf::Font *_fuente, sf::RenderWindow *_ventana)
                            : Pantalla(_pos_x,_pos_y,_largo,_altura,_fuente,_ventana){


}

void PantallaFinal::eventos(sf::Event evento, std::string &pantalla_actual) {

    if (evento.type == sf::Event::KeyReleased && pantalla_actual == "final"){
        if (evento.key.code == sf::Keyboard::Escape){
            ventana->close();
        }
    }
}

void PantallaFinal::dibujar() {

    ventana->clear(sf::Color::Black);           // Ventana se limpia con el color negro
    ventana->draw(texto_resultado);    // Se muestra el texto con el resultado
}

void PantallaFinal::actualizar_mensaje(std::string &ganador) {


    texto_resultado.setFont(*fuente);
    texto_resultado.setFillColor(sf::Color::Blue);
    texto_resultado.setCharacterSize(100);
    texto_resultado.setOrigin(texto_resultado.getGlobalBounds().width/2,texto_resultado.getGlobalBounds().height/2);
    texto_resultado.setPosition((float )pos_x,(float )pos_y);
    texto_resultado.setString("Ganador con 20 puntos - " + ganador );  // Mensaje con el ganador del Juego
}
