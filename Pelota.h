//
// Created by Christian on 20/07/21.
//

#ifndef PONG2021_PELOTA_H
#define PONG2021_PELOTA_H

#include <SFML/Graphics.hpp>
#include "Figura.h"

class Pelota : public Figura {
private:
    sf::CircleShape circulo;
    friend class PantallaJuego;

public:
    Pelota(int _largo,int _altura,sf::RenderWindow * _ventana);
    void mover() override;
    void dibujar() override;

};


#endif //PONG2021_PELOTA_H
