//
// Created by Christian on 20/07/21.
//

#ifndef PONG2021_PANTALLA_H
#define PONG2021_PANTALLA_H

#include <SFML//Graphics.hpp>

class Pantalla {
protected:
    int pos_x,pos_y,largo,altura;
    sf::Font * fuente;
    sf::RenderWindow * ventana;
public:
    Pantalla(int _pos_x,int _pos_y,int _largo,int _altura,sf::Font * _fuente,sf::RenderWindow * _ventana);
    virtual void eventos(sf::Event evento,std::string &pantalla_actual) = 0;
    virtual void dibujar() = 0;

};


#endif //PONG2021_PANTALLA_H
