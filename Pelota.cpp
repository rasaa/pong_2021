//
// Created by Christian on 20/07/21.
//

#include "Pelota.h"

Pelota::Pelota(int _largo, int _altura, sf::RenderWindow *_ventana)
                : Figura(_largo,_altura,_ventana){

    circulo.setRadius((float )largo);
    circulo.setFillColor(sf::Color(255,165,0));
    circulo.setOrigin(circulo.getGlobalBounds().width/2,circulo.getGlobalBounds().height/2);
}

void Pelota::mover() {

    // condiciones cuando toque arriba y abajo de la ventana
    if ( pos_y + des_y <= 0){
        des_y *= -1;
    }
    if ( pos_y + des_y + altura >= ventana->getSize().y){
        des_y *= -1;
    }

    // Desplazar la posicion x e y de la pelota
    pos_x += des_x;
    pos_y += des_y;
}

void Pelota::dibujar() {

    circulo.setPosition((float) pos_x,(float) pos_y);   // Establece la posicion
    ventana->draw(circulo);                                   // Muestra la pelota en pantalla
}
