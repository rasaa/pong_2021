//
// Created by Christian on 20/07/21.
//

#ifndef PONG2021_JUGADOR_H
#define PONG2021_JUGADOR_H

#include <SFML/Graphics.hpp>
#include "Figura.h"

class Jugador : public Figura {
private:
    sf::RectangleShape raqueta;
    int puntos;
    friend class PantallaJuego;
public:
    Jugador(int _largo,int _altura,sf::RenderWindow * _ventana);
    void mover() override;
    void dibujar() override;
    int get_puntos();

};


#endif //PONG2021_JUGADOR_H
