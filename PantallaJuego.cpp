//
// Created by Christian on 13/07/21.
//

#include "PantallaJuego.h"
#include "iostream"

PantallaJuego::PantallaJuego(int _pos_x, int _pos_y, int _largo, int _altura,
                             sf::Font * _fuente, sf::RenderWindow *_ventana)
                             : Pantalla(_pos_x,_pos_y,_largo,_altura,_fuente,_ventana),
                             jugador1(largo_figura,altura_figura,ventana),
                             jugador2(largo_figura,altura_figura,ventana),
                             pelota(radio_circulo,radio_circulo,ventana){

    // Malla de la mesa de Pong2021
    int malla_x = altura/160;
    int malla_y = altura/80;
    malla.setSize(sf::Vector2f((float)malla_x,(float)malla_y));
    malla.setOrigin(malla.getGlobalBounds().width/2,malla.getGlobalBounds().height/2);

    posicion_inicio();          // Posicion y velocidad de inicio de juego
}

void PantallaJuego::dibujar_malla() {

    for(int i=0; i < 40; ++i){
        malla.setPosition((float )pos_x,(float)altura/40*(float )i);
        ventana->draw(malla);
    }
}

void PantallaJuego::dibujar_puntos() {

    // Texto de escena2
    sf::Text texto_puntos1;
    sf::Text texto_puntos2;
    texto_puntos1.setFont(*fuente);
    texto_puntos2.setFont(*fuente);
    texto_puntos1.setString(std::to_string(jugador1.get_puntos()));     // de entero a string
    texto_puntos2.setString(std::to_string(jugador2.get_puntos()));     // de entero a string
    texto_puntos1.setCharacterSize(40);
    texto_puntos2.setCharacterSize(40);
    texto_puntos1.setFillColor(sf::Color::Red);
    texto_puntos2.setFillColor(sf::Color::Red);
    texto_puntos1.setOrigin(texto_puntos1.getGlobalBounds().width/2,texto_puntos1.getGlobalBounds().height/2);
    texto_puntos2.setOrigin(texto_puntos2.getGlobalBounds().width/2,texto_puntos2.getGlobalBounds().height/2);
    texto_puntos1.setPosition((float )largo/4,(float )altura/40);
    texto_puntos2.setPosition((float )largo*3/4,(float )altura/40);

    ventana->draw(texto_puntos1);       // Dibujar los puntos1
    ventana->draw(texto_puntos2);       // Dibujar los puntos2
}

void PantallaJuego::actualiza_puntos(std::string& ganador,std::string& pantalla_actual) {

    if (pelota.pos_x + pelota.des_x <= 0) {
        jugador2.puntos++;
        posicion_inicio();
    }
    if (jugador1.puntos == 20){
            ganador = "Jugador 1";
            pantalla_actual = "final";
        }
    if (pelota.pos_x + pelota.largo + pelota.des_x >= ventana->getSize().x){
        jugador1.puntos++;
        posicion_inicio();
    }
    if (jugador2.puntos == 20){
            ganador = "Jugador 2";
            pantalla_actual = "final";
        }
}

void PantallaJuego::actualizar() {

    // Mover Objetos de esta clase
    jugador1.mover();
    jugador2.mover();
    pelota.mover();
}

void PantallaJuego::dibujar() {

    // Dibujar en la Ventana
    ventana->clear(sf::Color(0,100,0));   // Limpiar la pantalla de Verde
    dibujar_malla();            // Mostrar la malla
    dibujar_puntos();           // Mostrar los puntos de cada jugador
    pelota.dibujar();           // Mostrar objeto pelota
    jugador1.dibujar();         // Mostrar objeto jugador1
    jugador2.dibujar();         // Mostrar objeto jugador2
}

void PantallaJuego::eventos(sf::Event evento, std::string &pantalla_actual) {

    if (evento.type == sf::Event::KeyPressed && pantalla_actual == "jugando"){
        if (evento.key.code == sf::Keyboard::Escape){
            pantalla_actual = "inicio";
        }
        if (evento.key.code == sf::Keyboard::Enter){
            pantalla_actual = "final";
        }
        if (evento.key.code == sf::Keyboard::A){
            jugador1.des_y -= velocidad_vertical;
        }
        if (evento.key.code == sf::Keyboard::Z){
            jugador1.des_y += velocidad_vertical;
        }
        if (evento.key.code == sf::Keyboard::K){
            jugador2.des_y -= velocidad_vertical;
        }
        if (evento.key.code == sf::Keyboard::M){
            jugador2.des_y += velocidad_vertical;
        }
    }
    if (evento.type == sf::Event::KeyReleased && pantalla_actual == "jugando"){
        if (evento.key.code == sf::Keyboard::A){
            jugador1.des_y = 0;
        }
        if (evento.key.code == sf::Keyboard::Z){
            jugador1.des_y = 0;
        }
        if (evento.key.code == sf::Keyboard::K){
            jugador2.des_y = 0;
        }
        if (evento.key.code == sf::Keyboard::M){
            jugador2.des_y = 0;
        }
    }
}

void PantallaJuego::detectar_colision() {

    // El operador >> equivale a " colisiona con"
    if (pelota >> jugador1)
        pelota.des_x *= -1;
    if (pelota >> jugador2)
        pelota.des_x *= -1;
}

void PantallaJuego::posicion_inicio() {

    // posicion de inicio de los objetos jugador1y2 y pelota
    jugador1.set_posicion(0,0);
    jugador2.set_posicion(largo-largo_figura,0);
    pelota.set_posicion(largo/2,0);

    // velocidad de pelota y jugadores para cada saque de pelota
    pelota.des_x = velocidad_horizontal * int (pow(-1,r()));
    pelota.des_y = velocidad_vertical;
    jugador1.des_x = 0;
    jugador1.des_y = 0;
    jugador2.des_x = 0;
    jugador2.des_y = 0;
}



