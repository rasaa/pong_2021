//
// Created by Christian on 20/07/21.
//

#ifndef PONG2021_FIGURA_H
#define PONG2021_FIGURA_H

#include <SFML//Graphics.hpp>

class Figura {
    friend bool operator >> (Figura& figura_a,Figura& figura_b);
protected:
    int pos_x,pos_y,largo,altura,des_x,des_y;
    sf::RenderWindow * ventana;
public:
    Figura(int _largo,int _altura,sf::RenderWindow * _ventana);
    virtual void mover() = 0 ;
    virtual void dibujar() = 0;
    void set_posicion(int _x,int _y);

};


#endif //PONG2021_FIGURA_H
