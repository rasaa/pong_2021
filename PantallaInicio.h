//
// Created by Christian on 13/07/21.
//

#ifndef PONG2021_PANTALLAINICIO_H
#define PONG2021_PANTALLAINICIO_H
#include <SFML/Graphics.hpp>
#include "Pantalla.h"

class PantallaInicio : public Pantalla{
private:
    sf::Text textoinicio;

public:
    PantallaInicio(int _pos_x,int _pos_y,int _largo,int _altura,sf::Font * _fuente,sf::RenderWindow * _ventana);
    void eventos(sf::Event evento,std::string &pantalla_actual) override;
    void dibujar() override;

};


#endif //PONG2021_PANTALLAINICIO_H
