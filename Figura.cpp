//
// Created by Christian on 20/07/21.
//

#include "Figura.h"

Figura::Figura( int _largo, int _altura, sf::RenderWindow * _ventana)
: pos_x(0),pos_y(0),largo(_largo),altura(_altura),des_x(0),des_y(0),ventana(_ventana){

}

void Figura::set_posicion(int _x, int _y) {
    pos_x = _x;
    pos_y = _y;
}

bool operator >> (Figura& figura_a,Figura& figura_b){

    // Se declaran los punteros para figura_a y figura_b
    Figura * pfa = &figura_a;
    Figura * pfb = &figura_b;

    // Se declaran las variables para los puntos de colision
    int arriba_a = pfa->pos_y;
    int arriba_b = pfb->pos_y;
    int abajo_a = arriba_a + pfa->altura;
    int abajo_b = arriba_b + pfb->altura;

    int izquierda_a = pfa->pos_x;
    int izquierda_b = pfb->pos_x;
    int derecha_a = izquierda_a + pfa->largo;
    int derecha_b = izquierda_b + pfb->largo;

    // Retornara falso si alguna de estas condiciones se cumplen
    return ! ( arriba_a > abajo_b || arriba_b > abajo_a ||
            izquierda_a > derecha_b || izquierda_b > derecha_a );
}