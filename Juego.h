//
// Created by Christian on 12/07/21.
//

#ifndef PONG2021_JUEGO_H
#define PONG2021_JUEGO_H
#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include "PantallaInicio.h"
#include "PantallaJuego.h"
#include "PantallaFinal.h"


class Juego {
private:
    // Se declara los atributos
    int pos_x,pos_y,largo,altura;
    sf::Font * fuente;
    sf::RenderWindow * ventana;
    PantallaInicio * escena1;
    PantallaJuego * escena2;
    PantallaFinal * escena3;
    sf::Event evento;
    std::string pantalla_actual;
    std::string ganador;

public:
    Juego();                // Se crea el Juego
    void inicializar();     // Inicializar parametros
    void ejecutar();        //  Bucle de Juego
    void actualizar();     // Actualizamos los objetos
    void adicionarpantalla(); // seleccion de escena
    ~Juego();               // Metodo Destructor

};


#endif //PONG2021_JUEGO_H
