//
// Created by Christian on 13/07/21.
//

#include "PantallaInicio.h"
#include <iostream>

PantallaInicio::PantallaInicio(int _pos_x,int _pos_y,int _largo,int _altura,sf::Font * _fuente, sf::RenderWindow * _ventana)
                            : Pantalla(_pos_x,_pos_y,_largo,_altura,_fuente,_ventana){

    // Declaracion de texto de escena1
    textoinicio.setFont(*fuente);
    textoinicio.setString("          PONG2021\n"
                           "\n\n"
                           "Presione Barra espaciadora para empezar");
    textoinicio.setFillColor(sf::Color::Yellow);
    textoinicio.setCharacterSize(100);
    textoinicio.setOrigin(textoinicio.getGlobalBounds().width/2,textoinicio.getGlobalBounds().height/2);
    textoinicio.setPosition((float )ventana->getSize().x/2,(float )ventana->getSize().y/2);

}

void PantallaInicio::eventos(sf::Event evento, std::string &pantalla_actual) {

    if (evento.type == sf::Event::KeyPressed && pantalla_actual == "inicio"){
        if (evento.key.code == sf::Keyboard::Space){
            pantalla_actual = "jugando";
        }
    }
}

void PantallaInicio::dibujar() {

    ventana->clear(sf::Color::Blue);    // Limpiar la pantalla
    ventana->draw(textoinicio);   // Mostar texto de Inicio

}
